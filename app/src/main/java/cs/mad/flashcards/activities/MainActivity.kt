package cs.mad.flashcards.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.ActionBar
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import androidx.recyclerview.widget.GridLayoutManager
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.entities.FlashcardSet


class MainActivity : AppCompatActivity() {

    private lateinit var adapter: FlashcardSetAdapter
    private lateinit var flashcardSets: MutableList<FlashcardSet>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val actionBar: ActionBar? = supportActionBar
        if (actionBar != null) {
            actionBar.hide()
        }
        val recyclerView : RecyclerView = findViewById(R.id.recyclerView)

        flashcardSets = FlashcardSet.getHardcodedFlashcardSets()
        adapter = FlashcardSetAdapter(flashcardSets)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = GridLayoutManager(this,2)

        adapter.onClick = { set ->

                onClick(set.title)

        }


    }

    fun onClick(flashcardSetTitle: String?){
        val intent = Intent(this,FlashcardSetDetailActivity::class.java).apply {
            putExtra("flashcardSetTitle", flashcardSetTitle)
        }
        startActivity(intent)

    }

    fun addSet(view: View){
        flashcardSets.add(FlashcardSet("testSet"))
        adapter.notifyItemInserted(flashcardSets.size-1)
    }


}