package cs.mad.flashcards.activities

import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardSetDetailAdapter
import cs.mad.flashcards.entities.Flashcard

class FlashcardSetDetailActivity : AppCompatActivity() {

    private lateinit var adapter: FlashcardSetDetailAdapter
    private lateinit var flashcards: MutableList<Flashcard>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activitiy_flashcard_main)

        val recyclerView: RecyclerView = findViewById(R.id.recyclerViewFlashcard)
        val textView: TextView = findViewById(R.id.setTitleTextView)
        val title = intent.getStringExtra("flashcardSetTitle")

        textView.text = title
        textView.typeface = Typeface.DEFAULT_BOLD


        flashcards = Flashcard.getHardcodedFlashcards()
        adapter = FlashcardSetDetailAdapter(flashcards)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter


        }

    private fun fetchData(): List<Flashcard> {

        return Flashcard.getHardcodedFlashcards()
    }

    fun addCard(view: View){
        flashcards.add(Flashcard("addTest","definition"))
        adapter.notifyItemInserted(flashcards.size-1)

    }
}