package cs.mad.flashcards.adapters

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.entities.Flashcard



class FlashcardSetDetailAdapter(
    private val item: List<Flashcard>
): RecyclerView.Adapter<FlashcardSetDetailAdapter.ViewHolder>(){

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val term: TextView = itemView.findViewById(R.id.termView)
        val def: TextView = itemView.findViewById(R.id.defView)
        init {

        }

    }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.activity_flashcard_set_detail,parent,false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int) {
        holder.term.typeface = Typeface.DEFAULT_BOLD
        holder.term.text = item[position].term
        holder.def.text = item[position].definition

    }

    override fun getItemCount(
    ): Int {
        return item.size
    }


}