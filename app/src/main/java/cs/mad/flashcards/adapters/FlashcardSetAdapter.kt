package cs.mad.flashcards.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.activities.FlashcardSetDetailActivity
import cs.mad.flashcards.entities.FlashcardSet

class FlashcardSetAdapter(
    private val item: List<FlashcardSet>
) : RecyclerView.Adapter<FlashcardSetAdapter.ViewHolder>() {
    var onClick: ((FlashcardSet) -> Unit)? = null


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val flashcardSet: TextView = itemView.findViewById(R.id.textView)
        init{
            flashcardSet.setOnClickListener{
                onClick?.invoke(item[adapterPosition])
            }
        }
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.activity_flashcard_set,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val set = item[position]

        holder.flashcardSet.text = set.title

    }

    override fun getItemCount(): Int {
        return item.size
    }


}