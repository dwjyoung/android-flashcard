1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="cs.mad.flashcards"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="24"
8-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml
9        android:targetSdkVersion="30" />
9-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml
10
11    <application
11-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml:5:5-26:19
12        android:allowBackup="true"
12-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml:6:9-35
13        android:appComponentFactory="androidx.core.app.CoreComponentFactory"
13-->[androidx.core:core:1.3.2] /Users/danielyoung/.gradle/caches/transforms-3/3fe674c9204bb76d8dc655be6ffac64d/transformed/core-1.3.2/AndroidManifest.xml:24:18-86
14        android:debuggable="true"
15        android:extractNativeLibs="false"
16        android:icon="@mipmap/ic_launcher"
16-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml:7:9-43
17        android:label="@string/app_name"
17-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml:8:9-41
18        android:roundIcon="@mipmap/ic_launcher_round"
18-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml:9:9-54
19        android:supportsRtl="true"
19-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml:10:9-35
20        android:testOnly="true"
21        android:theme="@style/Theme.Flashcards"
21-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml:11:9-48
22        android:windowSoftInputMode="adjustResize" >
22-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml:12:9-51
23        <activity
23-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml:13:9-18:67
24            android:name="cs.mad.flashcards.activities.FlashcardSetDetailActivity"
24-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml:14:13-66
25            android:exported="false"
25-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml:15:13-37
26            android:label="@string/title_activity_flashcard_set_detail"
26-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml:16:13-72
27            android:theme="@style/Theme.Flashcards.NoActionBar"
27-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml:18:13-64
28            android:windowSoftInputMode="stateHidden|adjustResize" />
28-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml:17:13-67
29        <activity android:name="cs.mad.flashcards.activities.MainActivity" >
29-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml:19:9-25:20
29-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml:19:19-58
30            <intent-filter>
30-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml:20:13-24:29
31                <action android:name="android.intent.action.MAIN" />
31-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml:21:17-69
31-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml:21:25-66
32
33                <category android:name="android.intent.category.LAUNCHER" />
33-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml:23:17-77
33-->/Users/danielyoung/School/CS422/Android/FlashcardAssignment/MADFlashcards/app/src/main/AndroidManifest.xml:23:27-74
34            </intent-filter>
35        </activity>
36    </application>
37
38</manifest>
