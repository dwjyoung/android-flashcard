// Generated by view binder compiler. Do not edit!
package cs.mad.flashcards.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import cs.mad.flashcards.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ActivityFlashcardSetDetailBinding implements ViewBinding {
  @NonNull
  private final CardView rootView;

  @NonNull
  public final TextView defView;

  @NonNull
  public final TextView termView;

  private ActivityFlashcardSetDetailBinding(@NonNull CardView rootView, @NonNull TextView defView,
      @NonNull TextView termView) {
    this.rootView = rootView;
    this.defView = defView;
    this.termView = termView;
  }

  @Override
  @NonNull
  public CardView getRoot() {
    return rootView;
  }

  @NonNull
  public static ActivityFlashcardSetDetailBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ActivityFlashcardSetDetailBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.activity_flashcard_set_detail, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ActivityFlashcardSetDetailBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.defView;
      TextView defView = ViewBindings.findChildViewById(rootView, id);
      if (defView == null) {
        break missingId;
      }

      id = R.id.termView;
      TextView termView = ViewBindings.findChildViewById(rootView, id);
      if (termView == null) {
        break missingId;
      }

      return new ActivityFlashcardSetDetailBinding((CardView) rootView, defView, termView);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
